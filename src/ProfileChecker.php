<?php

declare(strict_types=1);

namespace Drupal\enforce_profile;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Entity\EntityConstraintViolationList;
use Drupal\profile\Entity\Profile;
use Drupal\profile\Entity\ProfileInterface;

/**
 * @todo Add class description.
 */
final class ProfileChecker {

  /**
   * Constructs a ProfileChecker object.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly EntityFieldManagerInterface $entityFieldManager,
    private readonly AccountProxyInterface $currentUser,
  ) {}

  /**
   * @todo Add method description.
   */
  public function redirectProfile(): array {
    $redirect = [];
    $not_valid = [];
    
    // Get the current user id.
    $uid = $this->currentUser->id();
    // Get the profile storage.
    $storage_profile = $this->entityTypeManager->getStorage('profile');
    // Get the profile type storage.
    $storage_profile_type = $this->entityTypeManager->getStorage('profile_type');
    // Load all possible profile types.
    $profile_types = $storage_profile_type->loadMultiple();
    if (!empty($profile_types)) {
      foreach($profile_types as $profile_type) {
        $profile_type_array = $storage_profile->loadMultipleByUser($this->currentUser, $profile_type->id());
        if (!empty($profile_type_array)) {
          $user_profiles[] = $profile_type_array;
        }  
      }
    }
    
    if (isset($user_profiles) && !empty($user_profiles)) {
      foreach($user_profiles as $user_profile) {
        $profile_entity = reset($user_profile);
        if (
          $profile_entity instanceof Profile || 
          $profile_entity instanceof ProfileInterface
        ) {
          $valid = $profile_entity->validate();
          if ($valid instanceof EntityConstraintViolationList) {
            $unvalid_fields = $valid->getFieldNames();
            if (!empty($unvalid_fields)) {
              $not_valid[] = $profile_entity;
            }
          } 
        }
      }
    }

    return $not_valid;  
  }
}
