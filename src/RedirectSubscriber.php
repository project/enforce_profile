<?php

namespace Drupal\enforce_profile;

use Symfony\Component\HttpKernel\Event\RequestEvent;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\data_policy\Entity\DataPolicyInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\simple_oauth\Authentication\TokenAuthUserInterface;
use Drupal\enforce_profile\ProfileChecker;

/**
 * Redirection subscriber.
 *
 * @package Drupal\data_policy
 */
class RedirectSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The current active route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The redirect destination helper.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $destination;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The module handler interface.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  public $database;

  /**
   * The profile checker service.
   *
   * @var \Drupal\enforce_profile\ProfileChecker
   */
  protected $profileChecker;  

  /**
   * RedirectSubscriber constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current active route match object.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $destination
   *   The redirect destination helper.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler interface.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\enforce_profile\ProfileChecker $profile_checker
   *   The profile checker service.
   */
  public function __construct(
    RouteMatchInterface $route_match,
    RedirectDestinationInterface $destination,
    AccountProxyInterface $current_user,
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    MessengerInterface $messenger,
    ModuleHandlerInterface $module_handler,
    Connection $database,
    ProfileChecker $profile_checker
  ) {
    $this->routeMatch = $route_match;
    $this->destination = $destination;
    $this->currentUser = $current_user;
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->moduleHandler = $module_handler;
    $this->database = $database;
    $this->profileChecker = $profile_checker;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['checkForRedirection', '27'];
    return $events;
  }

  /**
   * This method is called when the KernelEvents::REQUEST event is dispatched.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event.
   */
  public function checkForRedirection(RequestEvent $event): void {
    // Check if the current route is of type profile edit form.
    // For now we leave the message.
    if (($route_name = $this->routeMatch->getRouteName()) === 'entity.profile.edit_form') {  
      // We don't need a redirect response. Let users know what to do!
      return;
    }

    // A non-Drupal UI application is making requests, lets leave the redirect to that
    // application instead, as we don't want to bring a dependency towards simple_oauth
    // we use the interface_exists to ensure it actually can be used.
    if (interface_exists(TokenAuthUserInterface::class) && $this->currentUser->getAccount() instanceof TokenAuthUserInterface) {
      return;
    }

    // People with the permission enforce profile allowance won't be redirected.
    if ($this->currentUser->hasPermission('enforce profile allowance')) {
      return;
    }
    
    // Check if the current route is the data policy agreement page.
    if (($route_name = $this->routeMatch->getRouteName()) === 'data_policy.data_policy.agreement') {
      // The current route is the data policy agreement page. We don't need
      // a redirect response.
      return;
    }
    
    // Check if the current route is the logout route. We do not want any redirection here.
    if (($route_name = $this->routeMatch->getRouteName()) === 'openid_connect.logout') {
      // The current route is the data policy agreement page. We don't need
      // a redirect response.
      return;
    }    
    
    // Check if the current route is the logout route. We do not want any redirection here.
    if (($route_name = $this->routeMatch->getRouteName()) === 'user.logout') {
      // The current route is the data policy agreement page. We don't need
      // a redirect response.
      return;
    }  

    $profile = $this->profileChecker->redirectProfile();
    
    if (!empty($profile)) {  
      // Get the first validation issue for 
      // the given profile.
      $entity = $profile[0];
      if ($entity->access('update', $this->currentUser)) {
        $this->doRedirect($event);
        return;
      }
    }    
  }

  /**
   * Do redirect to the agreement page.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event.
   */
  private function doRedirect(RequestEvent $event): void {

    $destination = $this->getDestination();

    // As this has already gone through method checkForRedirection()
    // we can assume the profile is set.
    $profile = $this->profileChecker->redirectProfile()[0];

    $url = Url::fromRoute('entity.profile.edit_form', ['profile' => $profile->id()], [
      'query' => $destination->getAsArray(),
    ]);

    $response = new RedirectResponse($url->toString());
    $event->setResponse($response);
  }

  /**
   * Get the redirect destination.
   *
   * @return \Drupal\Core\Routing\RedirectDestinationInterface
   *   The redirect destination.
   */
  protected function getDestination(): RedirectDestinationInterface {
    return $this->destination;
  }
}
