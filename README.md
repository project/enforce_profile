## INTRODUCTION

The Enforce Profile module is a small module helping you to enforce required profile fields to be filled in. It uses a RedirectSubscriber that will redirect to the profile of the user that is not filled out. If the user has more profiles, than the module will redirect to the affected profile edit page, as long as there is nothing left.

The primary use case for this module is:

- Redirect users when required profile fields are not set.

## REQUIREMENTS

- Profile drupal.org/project/profile

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

There is no need to configure this module, It will automatically detect the possible profile types.

## MAINTAINERS

Current maintainers for Drupal 10:

- Paul Mrvik (globexplorer) - https://www.drupal.org/u/globexplorer

